# Documentation Style Guide

## General

- Markdown
- Only document exported symbols

### Symbols

## Docstrings

Put a blank line between each section of the documentation.

### Conditions

Conditions can be grouped into two categories with respect to how they should be documented:

- base conditions for other conditions
- conditions that are intended to be signaled

The documentation for a condition starts with a phrase whose form depends on which categories it belongs to.

If it is a base condition, describe in that phrase that this class forms the base for a certain group of conditions. For example:

> Base condition for error conditions that are related to a specific test suite.

If it is a condition that is intended to be signaled, describe what its signaling indicates. For example:

> The argument provided for a test suite name parameter is invalid.

#### Slots

#### Description

### Functions

Start with a phrase that states succinctly what the function does. Write the Phrase in the imperative. State any side effects first followed by what is returned. For example:

> Print NAME to STREAM and return NAME as a string

#### Parameters

If the function takes arguments, add a list of its parameters and their types. Precede the list with the heading 'Parameter:' followed by a blank line. For each parameter add a line starting with the parameters name followed by a valid Common Lisp type specifier in parentheses. For example:

> Parameters:
>
> NAME (STRING-DESIGNATOR)
> STREAM ((and stream (satisfies output-stream-p)))

#### Description

If the initial phrase does not fully describe the functions behavior, add a complete description. Precede this with the heading 'Description:' followed by a blank line. For example:

> Description:
>
> Coerces NAME to a string, writes it to STREAM and returns it.

#### Errors

If the function may signal error conditions, add a list of these with descriptions of when they will be signaled. Do not list TYPE-ERROR conditions that are signaled due to arguments of the wrong type. Precede this with the heading 'Errors:' followed by a blank line. For example:

> Errors:
>
> INVALID-ARGUMENT-ERROR if coercing NAME to a string yield the empty string.
