# The Clean Project

The Clean project is a collection Common Lisp libraries and applications written by me. It is my pet project in which I live out my NIH syndrome. The name comes from the desire to create software as it should be created: with cleanly structured repositories, clean code, complete documentation, and all of that with a consistent style.

This repository contains the style guides I set myself for these projects. It is thus the basis of realizing a consistent style.
