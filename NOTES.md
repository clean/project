# Task List

- Report reporter dependency in :in-order-to clause
- Print strings in failed forms properly (~A vs ~S)
- Create a meta-class that allows to document the generic functions for reader,
  writer and accessors.
- Create a package that provides a reader syntax for non-printable (control)
  characters that is implementation independent and is based on unicode names
- Create a macro WITH-BODY-DESCRIPTION that dissects a BODY into a BODY and its
  optional description in form of a string as the first element.
- Create repository for helpful scripts:
  - find-todo.sh
  - gs.sh
  - notes.sh
- Create repository for this
- ```
  (defmacro with-struct-slots (type slots instance &body body)
    (with-gensyms (ginstance)
      `(let ((,ginstance ,instance))
         (symbol-macrolet
             ,(mapcar (lambda (slot)
                        (let ((var (if (symbolp slot) slot (car slot)))
                              (slot (if (symbolp slot) slot (cadr slot))))
                          `(,var (,(symbolicate type #\- slot) ,ginstance))))
                      slots)
           ,@body))))
  ```
